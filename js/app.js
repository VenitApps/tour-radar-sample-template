$(document).ready(function() {
    function fetchTours() {
        $.ajax({
            type: "GET",
            url: 'https://mocki.io/v1/11356aa2-6371-41d4-9d49-77a5e9e9924f',
            dataType: "json",
            success: function(response)
            {
                let template = Handlebars.templates.tour;
                $('.search-result-tours').html(template({tours: response}));
            },
            error: function()
            {
                $('.search-result-tours').append(
                    '<p class="no-result-info"><i class="smile-ico"></i>There is a problem with finding tours for you. We are working on it. Please try once again later.</p>'
                );
            }
        });
    }

    function initTemplateEngineSettings() {
        Handlebars.registerHelper("primaryTourImageUrl", function(images) {
            let primaryImageUrl;
            let firstAvailableImage;

            $.each(images, function (index, image) {
                if (image.url != null && image.url.length) {
                    firstAvailableImage = image.url;
                    if (image.is_primary === true) {
                        primaryImageUrl = image.url;
                    }
                }
            });

            return primaryImageUrl === undefined
                ? (firstAvailableImage === undefined ? './images/no-image.jpg' : firstAvailableImage.toString())
                : primaryImageUrl;
        });

        Handlebars.registerHelper("mark", function(mark) {
            if (typeof mark === 'number' && Number.isInteger(mark)) {
                return {'fullStar': mark, 'emptyStar': 5 - Math.floor(mark)};
            } else if (typeof mark === 'number') {
                return {'fullStar': Math.floor(mark), 'halfStar': 1, 'emptyStar': 5 - Math.floor(mark) - 1}
            }

            return {};
        });

        Handlebars.registerHelper("availability", function(dates) {
            let availabilityDates = [];

            if (!dates.length) {
                return [];
            }

            $.each(dates, function (index, date) {
                if (availabilityDates.length === 2) {
                    return false;
                }

                if (date.availability !== 0) {
                    availabilityDates.push({
                        'date': moment(date.start).format("DD MMM YYYY"),
                        'availability': date.availability,
                        'lowLimit': date.availability < 3
                    });
                }
            });

            return availabilityDates;
        });

        Handlebars.registerHelper("firstAndLastElementOfList", function(list) {
            let listLength = list.length;
            if (!listLength) {
                return {};
            }

            return {'first': list[0]?.name, 'last': listLength === 1 ? '' : list[listLength-1]?.name};
        });

        Handlebars.registerHelper("duration", function (length, type) {
            return (length + ' ' + (type === 'd' ? (length === 1 ? 'day' : 'days') : '')).toString();
        });

        Handlebars.registerHelper("citiesNames", function (cities) {
            if (!cities.length) {
                return {}
            }

            let first = cities[0]?.name;
            let second = cities.length === 1 ? null : cities[1]?.name;
            let more = cities.length === 2 ? null : cities.length - 1;

            return {first, second, more};
        });

        Handlebars.registerHelper("priceAndDiscount", function (dates) {
            let lowestPrice;
            let discount;

            $.each(dates, function (index, departureDate) {
                if (lowestPrice === undefined) {
                    lowestPrice = departureDate.eur;
                    discount = departureDate.discount;
                }

                if (departureDate.eur < lowestPrice) {
                    lowestPrice = departureDate.eur;
                    discount = departureDate.discount;
                }

                if (departureDate.eur === lowestPrice) {
                    if (departureDate.discount != null && departureDate.discount.length) {
                        if (discount === undefined) {
                            discount = departureDate.discount;
                            return;
                        }
                        
                        if (parseInt(departureDate.discount.slice(0, -1)) > parseInt(discount.slice(0, -1))) {
                            discount = departureDate.discount;
                        }
                    }
                }
            });

            return { lowestPrice, discount }
        });

        Handlebars.registerHelper('isdefined', function (value) {
            return value !== undefined;
        });

        Handlebars.registerHelper('times', function(n, block) {
            let accum = '';

            for (let i = 0; i < n; ++i)
                accum += block.fn(i);

            return accum;
        });
    }

    function sortData(type) {
        let searchResultTours = $('.search-result-tours');

        searchResultTours.find('.tour').sort(function (firstTour, secondTour) {
            switch (type) {
                case 'highest-price':
                    return $(secondTour).find('.tour-price-from span').data('price') - $(firstTour).find('.tour-price-from span').data('price');
                case 'lowest-price':
                    return $(firstTour).find('.tour-price-from span').data('price') - $(secondTour).find('.tour-price-from span').data('price');
                case 'longest-tour':
                    return $(secondTour).find('.tour-duration-days').data('duration') - $(firstTour).find('.tour-duration-days').data('duration');
                case 'shortest-tour':
                    return $(firstTour).find('.tour-duration-days').data('duration') - $(secondTour).find('.tour-duration-days').data('duration');
            }

        }).appendTo(searchResultTours);
    }

    function filterBy(month) {
        let tours = $('.search-result-tours .tour');
        let searchResultTours = $('.search-result-tours');
        let noResultInfo = $('.no-result-info');

        if (!tours.length) {
            return;
        }

        tours.show();
        noResultInfo.remove();

        if (!month.length) {
            return;
        }

        $.each(tours, function (index, tour) {
            let tourDates = [];
            $.each($(tour).find('.tour-available-dates dt'), function (index, item) {
                tourDates.push($(item).text());
            });

            if (!tourDates.length) {
                $(this).hide();
                return;
            }

            let filteredTourByDate = tourDates.filter(
                value => moment(value).isValid() && moment(value).format('MMM') === month
            );

            if (!filteredTourByDate.length) {
                $(tour).hide();
            }
        });

        if (tours.is(':visible') === false) {
            searchResultTours.append(
                '<p class="no-result-info"><i class="smile-ico"></i>There is no result for provided criteria.</p>'
            );
        }
    }

    initTemplateEngineSettings();
    fetchTours();

    let select = $('select');
    select.niceSelect();

    $('select[name="sortBy"]').change(function() {
        sortData($("#sortBy").val());
    });

    $('select[name="filterBy"]').change(function() {
        filterBy($("#filterBy").val());
    });
});
