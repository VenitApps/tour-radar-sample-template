(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tour'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, alias4="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"tour\">\r\n        <div class=\"tour-image\">\r\n            <img src=\""
    + alias3((lookupProperty(helpers,"primaryTourImageUrl")||(depth0 && lookupProperty(depth0,"primaryTourImageUrl"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"images") : depth0),{"name":"primaryTourImageUrl","hash":{},"data":data,"loc":{"start":{"line":4,"column":22},"end":{"line":4,"column":54}}}))
    + "\" alt=\""
    + alias3(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias4 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":4,"column":61},"end":{"line":4,"column":71}}}) : helper)))
    + " image\" title=\""
    + alias3(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias4 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":4,"column":86},"end":{"line":4,"column":96}}}) : helper)))
    + "\">\r\n        </div>\r\n        <div class=\"tour-description\">\r\n            <h2 class=\"tour-name\">"
    + alias3(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias4 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":7,"column":34},"end":{"line":7,"column":44}}}) : helper)))
    + "</h2>\r\n            <div class=\"tour-rate\">\r\n                <ul class=\"tour-rate-stars\">\r\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(lookupProperty(helpers,"mark")||(depth0 && lookupProperty(depth0,"mark"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"rating") : depth0),{"name":"mark","hash":{},"data":data,"loc":{"start":{"line":10,"column":28},"end":{"line":10,"column":41}}}),{"name":"with","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":10,"column":20},"end":{"line":26,"column":29}}})) != null ? stack1 : "")
    + "                </ul>\r\n\r\n                <span class=\"tour-rate-reviews-counter\">\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"reviews") : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data,"loc":{"start":{"line":30,"column":20},"end":{"line":34,"column":27}}})) != null ? stack1 : "")
    + "                </span>\r\n            </div>\r\n\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"reviews") : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":38,"column":12},"end":{"line":42,"column":19}}})) != null ? stack1 : "")
    + "            <div class=\"tour-description-details\">\r\n                <dl>\r\n                    <dt>Destinations</dt>\r\n                    <dd>\r\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(lookupProperty(helpers,"citiesNames")||(depth0 && lookupProperty(depth0,"citiesNames"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"cities") : depth0),{"name":"citiesNames","hash":{},"data":data,"loc":{"start":{"line":47,"column":32},"end":{"line":47,"column":52}}}),{"name":"with","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":47,"column":24},"end":{"line":50,"column":33}}})) != null ? stack1 : "")
    + "                    </dd>\r\n                </dl>\r\n                <dl>\r\n                    <dt>Starts / Ends in</dt>\r\n                    <dd>\r\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(lookupProperty(helpers,"firstAndLastElementOfList")||(depth0 && lookupProperty(depth0,"firstAndLastElementOfList"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"cities") : depth0),{"name":"firstAndLastElementOfList","hash":{},"data":data,"loc":{"start":{"line":56,"column":32},"end":{"line":56,"column":66}}}),{"name":"with","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":56,"column":24},"end":{"line":58,"column":33}}})) != null ? stack1 : "")
    + "                    </dd>\r\n                </dl>\r\n                <dl>\r\n                    <dt>Operator</dt>\r\n                    <dd>"
    + alias3(((helper = (helper = lookupProperty(helpers,"operator_name") || (depth0 != null ? lookupProperty(depth0,"operator_name") : depth0)) != null ? helper : alias2),(typeof helper === alias4 ? helper.call(alias1,{"name":"operator_name","hash":{},"data":data,"loc":{"start":{"line":63,"column":24},"end":{"line":63,"column":43}}}) : helper)))
    + "</dd>\r\n                </dl>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"tour-price-and-availability\">\r\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(lookupProperty(helpers,"priceAndDiscount")||(depth0 && lookupProperty(depth0,"priceAndDiscount"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"dates") : depth0),{"name":"priceAndDiscount","hash":{},"data":data,"loc":{"start":{"line":69,"column":20},"end":{"line":69,"column":44}}}),{"name":"with","hash":{},"fn":container.program(27, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":69,"column":12},"end":{"line":75,"column":21}}})) != null ? stack1 : "")
    + "\r\n            <div class=\"tour-duration-and-price\">\r\n                <dl class=\"tour-duration\">\r\n                    <dt class=\"tour-duration-label\">Duration</dt>\r\n                    <dd class=\"tour-duration-days\" data-duration=\""
    + alias3(((helper = (helper = lookupProperty(helpers,"length") || (depth0 != null ? lookupProperty(depth0,"length") : depth0)) != null ? helper : alias2),(typeof helper === alias4 ? helper.call(alias1,{"name":"length","hash":{},"data":data,"loc":{"start":{"line":80,"column":66},"end":{"line":80,"column":76}}}) : helper)))
    + "\">\r\n                        "
    + alias3((lookupProperty(helpers,"duration")||(depth0 && lookupProperty(depth0,"duration"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"length") : depth0),(depth0 != null ? lookupProperty(depth0,"length_type") : depth0),{"name":"duration","hash":{},"data":data,"loc":{"start":{"line":81,"column":24},"end":{"line":81,"column":57}}}))
    + "\r\n                    </dd>\r\n                </dl>\r\n                <dl class=\"tour-price\">\r\n                    <dt class=\"tour-price-label\">From</dt>\r\n                    <dd class=\"tour-price-from\">\r\n"
    + ((stack1 = lookupProperty(helpers,"with").call(alias1,(lookupProperty(helpers,"priceAndDiscount")||(depth0 && lookupProperty(depth0,"priceAndDiscount"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"dates") : depth0),{"name":"priceAndDiscount","hash":{},"data":data,"loc":{"start":{"line":87,"column":32},"end":{"line":87,"column":56}}}),{"name":"with","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":87,"column":24},"end":{"line":93,"column":33}}})) != null ? stack1 : "")
    + "                    </dd>\r\n                </dl>\r\n            </div>\r\n            <hr>\r\n            <div class=\"tour-available-dates\">\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(lookupProperty(helpers,"availability")||(depth0 && lookupProperty(depth0,"availability"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"dates") : depth0),{"name":"availability","hash":{},"data":data,"loc":{"start":{"line":99,"column":24},"end":{"line":99,"column":44}}}),{"name":"each","hash":{},"fn":container.program(35, data, 0),"inverse":container.program(39, data, 0),"data":data,"loc":{"start":{"line":99,"column":16},"end":{"line":113,"column":25}}})) != null ? stack1 : "")
    + "            </div>\r\n            <button class=\"tour-preview-button tour-preview-button--orange\">\r\n                View tour\r\n                <i class=\"tour-preview-button-arrow\"></i>\r\n            </button>\r\n        </div>\r\n    </div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"fullStar") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(10, data, 0),"data":data,"loc":{"start":{"line":11,"column":24},"end":{"line":25,"column":31}}})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = (lookupProperty(helpers,"times")||(depth0 && lookupProperty(depth0,"times"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"fullStar") : depth0),{"name":"times","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":12,"column":28},"end":{"line":14,"column":38}}})) != null ? stack1 : "")
    + ((stack1 = (lookupProperty(helpers,"times")||(depth0 && lookupProperty(depth0,"times"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"halfStar") : depth0),{"name":"times","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":15,"column":28},"end":{"line":17,"column":38}}})) != null ? stack1 : "")
    + ((stack1 = (lookupProperty(helpers,"times")||(depth0 && lookupProperty(depth0,"times"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"emptyStar") : depth0),{"name":"times","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":18,"column":28},"end":{"line":20,"column":38}}})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "                                <li class=\"tour-rate-star tour-rate-star--full\"></li>\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "                                <li class=\"tour-rate-star tour-rate-star--half\"></li>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "                                <li class=\"tour-rate-star tour-rate-star--empty\"></li>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = (lookupProperty(helpers,"times")||(depth0 && lookupProperty(depth0,"times"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),5,{"name":"times","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":28},"end":{"line":24,"column":38}}})) != null ? stack1 : "");
},"12":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"reviews") || (depth0 != null ? lookupProperty(depth0,"reviews") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"reviews","hash":{},"data":data,"loc":{"start":{"line":31,"column":24},"end":{"line":31,"column":37}}}) : helper)))
    + " reviews\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "                        no reviews yet - <a href=\"#\" title=\"be the first one\">be the first one</a>\r\n";
},"16":function(container,depth0,helpers,partials,data) {
    return "                <div class=\"user-comment\">\r\n                    <q>My husband and I went on this trip and we must admin, this the one of the best vacation</q>\r\n                </div>\r\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isdefined")||(depth0 && lookupProperty(depth0,"isdefined"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"first") : depth0),{"name":"isdefined","hash":{},"data":data,"loc":{"start":{"line":48,"column":34},"end":{"line":48,"column":51}}}),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":48,"column":28},"end":{"line":48,"column":70}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isdefined")||(depth0 && lookupProperty(depth0,"isdefined"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"second") : depth0),{"name":"isdefined","hash":{},"data":data,"loc":{"start":{"line":48,"column":76},"end":{"line":48,"column":94}}}),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":48,"column":70},"end":{"line":48,"column":117}}})) != null ? stack1 : "")
    + "\r\n                            "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isdefined")||(depth0 && lookupProperty(depth0,"isdefined"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"more") : depth0),{"name":"isdefined","hash":{},"data":data,"loc":{"start":{"line":49,"column":34},"end":{"line":49,"column":50}}}),{"name":"if","hash":{},"fn":container.program(23, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":49,"column":28},"end":{"line":49,"column":118}}})) != null ? stack1 : "")
    + "\r\n";
},"19":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression(((helper = (helper = lookupProperty(helpers,"first") || (depth0 != null ? lookupProperty(depth0,"first") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"first","hash":{},"data":data,"loc":{"start":{"line":48,"column":53},"end":{"line":48,"column":63}}}) : helper)));
},"21":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ", "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"second") || (depth0 != null ? lookupProperty(depth0,"second") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"second","hash":{},"data":data,"loc":{"start":{"line":48,"column":98},"end":{"line":48,"column":110}}}) : helper)));
},"23":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return " <a href=\"#\" title=\"+"
    + alias4(((helper = (helper = lookupProperty(helpers,"more") || (depth0 != null ? lookupProperty(depth0,"more") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"more","hash":{},"data":data,"loc":{"start":{"line":49,"column":73},"end":{"line":49,"column":83}}}) : helper)))
    + " more\">+"
    + alias4(((helper = (helper = lookupProperty(helpers,"more") || (depth0 != null ? lookupProperty(depth0,"more") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"more","hash":{},"data":data,"loc":{"start":{"line":49,"column":91},"end":{"line":49,"column":101}}}) : helper)))
    + " more</a> ";
},"25":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            "
    + alias4(((helper = (helper = lookupProperty(helpers,"first") || (depth0 != null ? lookupProperty(depth0,"first") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"first","hash":{},"data":data,"loc":{"start":{"line":57,"column":28},"end":{"line":57,"column":39}}}) : helper)))
    + " / "
    + alias4(((helper = (helper = lookupProperty(helpers,"last") || (depth0 != null ? lookupProperty(depth0,"last") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"last","hash":{},"data":data,"loc":{"start":{"line":57,"column":42},"end":{"line":57,"column":52}}}) : helper)))
    + "\r\n";
},"27":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isdefined")||(depth0 && lookupProperty(depth0,"isdefined"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"discount") : depth0),{"name":"isdefined","hash":{},"data":data,"loc":{"start":{"line":70,"column":22},"end":{"line":70,"column":42}}}),{"name":"if","hash":{},"fn":container.program(28, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":70,"column":16},"end":{"line":74,"column":23}}})) != null ? stack1 : "");
},"28":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    <div class=\"tour-price-discount\">\r\n                        <span class=\"tour-price-discount-value\">-"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"discount") || (depth0 != null ? lookupProperty(depth0,"discount") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"discount","hash":{},"data":data,"loc":{"start":{"line":72,"column":65},"end":{"line":72,"column":79}}}) : helper)))
    + "</span>\r\n                    </div>\r\n";
},"30":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(lookupProperty(helpers,"isdefined")||(depth0 && lookupProperty(depth0,"isdefined"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"lowestPrice") : depth0),{"name":"isdefined","hash":{},"data":data,"loc":{"start":{"line":88,"column":34},"end":{"line":88,"column":57}}}),{"name":"if","hash":{},"fn":container.program(31, data, 0),"inverse":container.program(33, data, 0),"data":data,"loc":{"start":{"line":88,"column":28},"end":{"line":92,"column":35}}})) != null ? stack1 : "");
},"31":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                                <span data-price=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"lowestPrice") || (depth0 != null ? lookupProperty(depth0,"lowestPrice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lowestPrice","hash":{},"data":data,"loc":{"start":{"line":89,"column":50},"end":{"line":89,"column":67}}}) : helper)))
    + "\">€"
    + alias4(((helper = (helper = lookupProperty(helpers,"lowestPrice") || (depth0 != null ? lookupProperty(depth0,"lowestPrice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lowestPrice","hash":{},"data":data,"loc":{"start":{"line":89,"column":70},"end":{"line":89,"column":87}}}) : helper)))
    + "</span>\r\n";
},"33":function(container,depth0,helpers,partials,data) {
    return "                                <span data-price=\"0\">--</span>\r\n";
},"35":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"with").call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"with","hash":{},"fn":container.program(36, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":100,"column":20},"end":{"line":107,"column":29}}})) != null ? stack1 : "");
},"36":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                        <dl>\r\n                            <dt>"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"date") : depth0), depth0))
    + "</dt>\r\n                            <dd "
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"lowLimit") : depth0),{"name":"if","hash":{},"fn":container.program(37, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":103,"column":32},"end":{"line":103,"column":89}}})) != null ? stack1 : "")
    + ">\r\n                                "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"availability") : depth0), depth0))
    + " space left\r\n                            </dd>\r\n                        </dl>\r\n";
},"37":function(container,depth0,helpers,partials,data) {
    return "class=\"low-tour-availability\"";
},"39":function(container,depth0,helpers,partials,data) {
    return "                    <div class=\"no-tour-available-dates\">\r\n                        <p class=\"no-free-dates\">No free dates</p>\r\n                        <a href=\"#\" title=\"inform me when available\">inform me when available</a>\r\n                    </div>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"tours") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":121,"column":9}}})) != null ? stack1 : "")
    + "\r\n";
},"useData":true});
})();