# README #

Here is repository of TourRadar sample SERP page

### Question answers ###

* How would you ensure that the page is fully responsive and developed with mobile-first in mind?**

  Template was build in mobile-first technique. All css entries are dedicated for mobile view. Going further with @media break point (I have provided only one - but there could be more) I am able to provide additional css rules that adjust view for bigger screens eg. screen of tablet or desktop. 

* **What sort of optimizations would you suggest for better UI/UX? Are there any tools/metrics that can help to achieve and/or measure these goals?**
  
  - There could be done some A/B test with users and present several views for choosing the best one
  - There could be done tests of appropriate elements color contrast, to assure that everything is visible properly
  - There could be done adjustments to provide good semantic html code that makes website available for disable users (eg. code consistent with WCAG 2.1)
  - There could be added some UI animations which provide better reception and user experience
  - There could be added gamification techniques for better connection with users and potential customers.
  
* **How would you make sure that the page works as expected on all supported browsers/devices?**

  There are a few applications eg. www.browserstack.com or https://www.lambdatest.com that enable tests provided URL in selected versions of web browsers.
  There could be also provided web test URL to make tests in real devices eg. phones. First tests could be done using dev tools build in the browser. 
  
* **What criteria would you consider for choosing the tech stack for this test case?**

  Deciding what stack to choose I have considered things below:
  - weight of external libraries
  - complexity - I prefer writing simply, small, maintainable code that meet requirements
  - if library support mobile devices


### About solution ###
I have decided to use those external libraries

- Handlebar - Minimal templating library for JS. Its semantics is similar to blade templates knowing from Laravel. Template is located in js/template folder and contain logic for displaying information fetched from the server. To speed up process of rendering and displaying in PROD stage I have precompile template. 
- Moment.js - Used to adjust displaying date in templates
- Nice select - Library to stylize dropdown selects
- jQuery - Library to use JS in better manner. 

Custom JS code is located in app.js file. There are custom Handlebar functions that are used in a template. They provide first data sanitizing and logic for some missing data. Of course this logic can be different taking into consideration business rules.

### Todo ###
 - there can be more @media breakpoint to provide better interface for different screen sizes
 - there can be done minification and combination of all CSS and JS files to speed up processing on PROD stage
 - code can be written in TypeScript to avoid errors and mistakes at development stage. It can be translated into small JS file for a PROD stage. This also provide better code maintenance.
 - data fetched from the server can be converted into objects collections (eg. collection of Tours) which can encapsulate some logic and can contain smaller portion of necessary data
 
### What has not been done ###
I did not test if solution meet browser versions requirements. I do not have possibility to do that. I do not have access to appropriate tools eg. mentioned above.

### Time ###

From my point of view the time for this task should be longer than 2-4 hours. Considering proper libraries, reading documentations, writing additional custom data processing functions, functions for dynamic generation of template elements, preparing template skeleton and icons, testing view in all resolutions, manually testing for looking bugs etc. took me around 16-20h.  
